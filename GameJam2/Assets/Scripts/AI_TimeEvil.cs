﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace ARTPATH.AI
{
    public class AI_TimeEvil : MonoBehaviour
    {        
        [Title("Set-up")] 
        public Animator animator;
        
        private Rigidbody2D rb;

        private GameObject player;

        [FoldoutGroup("AI Setting", true, 0)] public int HP = 3;
        [FoldoutGroup("AI Setting", true, 0)] public float speed;
        [FoldoutGroup("AI Setting", true, 0)] public float jumpForce;
        [FoldoutGroup("AI Setting", true, 0)] public float height;
        [FoldoutGroup("AI Setting", true, 0)] public Transform ForwardDetector;
        [FoldoutGroup("AI Setting", true, 0)] public float ForwardGroundDetectionDistance;
        [FoldoutGroup("AI Setting", true, 0)] public float ForwardObstacleDetectionDistance;
        [FoldoutGroup("AI Setting", true, 0)] public float ForwardRoofDetectionDistance;
        [FoldoutGroup("AI Setting", true, 0)] public Transform CenterDetector;
        [FoldoutGroup("AI Setting", true, 0)] public float CenterGroundDetectionRadius;
        [FoldoutGroup("AI Setting", true, 0)] public float CenterRoofDetectionDistance;
        [FoldoutGroup("AI Setting", true, 0)] public float JumpCD;
        [FoldoutGroup("AI Setting", true, 0)] public LayerMask groundMask;
        [FoldoutGroup("AI Setting", true, 0)] public LayerMask LineOfSightMask;
        [FoldoutGroup("AI Setting", true, 0)] public float DetectingDistance;
        [FoldoutGroup("AI Setting", true, 0)] public float ChargeDelay;
        [FoldoutGroup("AI Setting", true, 0)] public float ChargeSpeed;
        [FoldoutGroup("AI Setting", true, 0)] public float ChargeDuration;
        [FoldoutGroup("AI Setting", true, 0)] public float limitedVelocity;
        private float nextJump = 1;
        private float nextCharge = 2;
        private float ChargeDurationRemain;
        private bool isGrounded = false;
        private float MobilityPercentage = 100;

        private Color DebugLineOfSight;

        private void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
            player = GameObject.Find("PlayerLocation");
            ChargeDurationRemain = ChargeDuration;
        }

        private void Update()
        {
            isGrounded = Physics2D.OverlapCircle(CenterDetector.position, CenterGroundDetectionRadius, groundMask);

            Vector3 dir = player.transform.position - this.transform.position;
            RaycastHit2D PlayerInfo = Physics2D.Raycast(transform.position,
                dir,
                Mathf.Infinity, LineOfSightMask);

            Debug.Log(ChargeDurationRemain);
            if (ChargeDurationRemain > 0)
            {
                Charging();
                if (ChargeDurationRemain <= 0)
                {
                    nextCharge = Time.time + ChargeDelay;
                }
                DebugLineOfSight = Color.red;
            }
            else if (PlayerInfo.distance <= DetectingDistance && PlayerInfo.collider.CompareTag("Player"))
            {
                DebugLineOfSight = Color.yellow;
                if (Time.time > nextCharge)
                {
                    ChargeDurationRemain = ChargeDuration;
                }
                else
                {
                    rb.gravityScale = 2;
                    DefaultChasingMechanic();
                }
            }
            else
            {
                rb.gravityScale = 2;
                DebugLineOfSight = Color.gray;
                DefaultChasingMechanic();
                animator.SetBool("IsDash",false);
            }

            Debug.DrawRay(transform.position, player.transform.position - this.transform.position,
                DebugLineOfSight);


            Debug.DrawRay(ForwardDetector.position, Vector2.down * ForwardGroundDetectionDistance,
                Color.green);
            Debug.DrawRay(ForwardDetector.position, -transform.right * ForwardObstacleDetectionDistance,
                Color.green);
            Debug.DrawRay(ForwardDetector.position, Vector2.up * ForwardRoofDetectionDistance,
                Color.green);
            
            if (Mathf.Abs(rb.velocity.x) > limitedVelocity && ChargeDurationRemain <= 0 
                                                           && PlayerInfo.distance <= DetectingDistance 
                                                           && !PlayerInfo.collider.CompareTag("Player"))
            {
                rb.AddForce(new Vector2(-rb.velocity.x, 0));
            }
        }

        private void DefaultChasingMechanic()
        {
            if (player != null)
            {
                if (player.transform.position.x - transform.position.x > 0)
                {
                    this.transform.rotation = Quaternion.Euler(0, 180, 0);
                }
                else
                {
                    this.transform.rotation = Quaternion.Euler(0, 0, 0);
                }

                RaycastHit2D groundInfo = Physics2D.Raycast(ForwardDetector.position, Vector2.down,
                    ForwardGroundDetectionDistance, groundMask);
                RaycastHit2D obstacleInfo = Physics2D.Raycast(ForwardDetector.position, Vector2.right,
                    ForwardObstacleDetectionDistance, groundMask);
                RaycastHit2D roofInfo = Physics2D.Raycast(ForwardDetector.position, Vector2.up,
                    ForwardRoofDetectionDistance, groundMask);
                RaycastHit2D aboveHeadInfo = Physics2D.Raycast(CenterDetector.position + Vector3.up, Vector2.up,
                    CenterRoofDetectionDistance, groundMask);

                rb.AddForce(new Vector2((player.transform.position.x - transform.position.x), 0).normalized * speed *
                            Time.deltaTime * MobilityPercentage);

                if ((groundInfo.collider && !obstacleInfo.collider &&
                     Mathf.Abs(transform.position.x - player.transform.position.x) >
                     transform.position.y - player.transform.position.y - height &&
                     player.transform.position.y - transform.position.y < height))
                {
                    MobilityPercentage = 100;
                }
                else if (Time.time > nextJump && isGrounded && player.transform.position.y - transform.position.y > height)
                {
                    if (!roofInfo.collider && Mathf.Abs(transform.position.x - player.transform.position.x) <
                        transform.position.y - player.transform.position.y - height)
                    {
                        MobilityPercentage = 70;
                        nextJump = Time.time + JumpCD;
                        rb.velocity = Vector2.up * jumpForce;
                    }
                    else if (!aboveHeadInfo.collider)
                    {
                        MobilityPercentage = 70;
                        nextJump = Time.time + JumpCD;
                        rb.velocity = Vector2.up * jumpForce;
                    }
                }
                else
                {
                    MobilityPercentage = 100;
                }
            }
        }

        private void Charging()
        {
            animator.SetBool("IsDash",true);
            rb.gravityScale = 0;
            ChargeDurationRemain -= Time.deltaTime;
            Vector2 dir = player.transform.position - this.transform.position;
            rb.velocity = dir.normalized * ChargeSpeed;
        }
    }
}